@extends('layouts.app')

@section('heading', 'Pertanyaan')

@section('button')
  <div class="col-sm-2 my-auto">
    <a class="btn btn-primary" href="{{ route('questions.create') }}">Bertanya</a>
  </div>
@endsection

@section('content')
  <div class="list-group">
    @forelse ($questions ?? '' as $key => $question)
      <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
          <h5 class="mb-1">{{ $question->title }}</h5>
          <small class="text-muted">Waktu Pembuatan</small>
        </div>
        <p class="mb-1">{{ $question->body }}</p>
        <small class="text-muted">Author</small>
      </a>
    @empty
      <h5>No Data</h5>
    @endforelse
  </div>
@endsection
