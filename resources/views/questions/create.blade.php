@extends('layouts.app')

@section('heading', 'Buat Pertanyaan')

@section('content')
  <form class="" action="{{ route('questions.store') }}" method="post">
    @csrf
    <div class="form-group">
      <label class="col-form-label col-form-label-lg" for="inputLarge">Judul</label>
      <input class="form-control form-control-lg" name="title" type="text" placeholder="Tulis Judul" value="{{ old('title') }}" id="inputLarge">
    </div>
    <div class="form-group">
      <label for="exampleTextarea">Isi Pertanyaan</label>
      <textarea class="form-control" name="body" id="exampleTextarea" rows="3">{{ old('body') }}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
